/**
 *
 */
package program;
import java.util.Scanner;
/**
 * @author katsu tandoku
 *
 */
public class UAVbasic {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//-------------------------------要素定義----------------------------------------------------------------------
		Scanner stdIn = new Scanner(System.in);

		System.out.println("UAVのデータを入力せよ。");
		System.out.print("残バッテリー量は(Maxバッテリー=39963) : ");            double battery = stdIn.nextDouble();

		//------------------------------バッテリ-----------------------------------------------------------------------
		double capa = 39963; //電力量ws
		double cons = 67.082; //1sあたりに消費する電力w 基本 66.314+0.768(アイドリング)
		/*v=8 cons = 68.49 帰還 出発 +0.768(アイドリング)
		  v=0 cons = 65.258 ホバリング +0.768(アイドリング)*/
		double hobacons = 69;//ホバリング時消費量
		double plusbattery = 55.5;//1sあたり充電される電力w

		UAV[]  myUAV = new UAV[1];
		double x = 0.0;
		double y = 0.0;
		int kikan = 0;
		int move = 0;
		int movecount =1;
		int hobacount =1;
		int sensingcount = 1;
		int sensingtime = 0;
		int photo = 0;
		int photocount = 0;


		for(int a=0; a<myUAV.length; a++) {
		myUAV[a] = new UAV(x, y, capa, cons, battery, kikan, move, photo, photocount);
		}


		myUAV[0].x =-285.0; myUAV[0].y = 50.0;
		myUAV[0].move=1;

		hoba[] myHOBA = new hoba[1];
		double hobax = 0.0;
		double hobay = 0.0;
		myHOBA[0] = new hoba(hobax, hobay);
		myUAV[0].photo =1;

		double alltime =0;//総飛行時間
		double dx = 0;
		double dy = 0;
		int ymode = 1;//y進み方
		int kyorimode = 1;
		int i = 0;//myUAVのID
		//int z = 0;//threshould判断の変数
		int hoba = 0;//ホバリング
		int thre = 0;//閾値
		int k = 0;
		int l = 0;
		int L=0;
		int right = 0;
		int rightcount =0;
		int allphoto = 0;//基地局帰還したデータ枚数
		correct[] correct = new correct[30];
		double Ctime = 0.0; int Allphoto = 0;
		for(int a=0; a<correct.length; a++) {
		correct[a] = new correct(Ctime, Allphoto);
		}
		int c = 0; //収集に使う関数
		int kannsoku = 1;




		/*初期状態の各UAV*/
		System.out.println("--------------------------------");
		for(i=0; i<myUAV.length; i++) {
			System.out.println("No" +(i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + ")バッテリー" + myUAV[i].getFuel() + "画像枚数" +myUAV[i].getPhoto());		}
		System.out.println("--------------------------------");



		/*残バッテリー>MAXバッテリーのエラー処理*/
		for(i=0; i<myUAV.length; i++) {
			do {
				if (!myUAV[i].over(capa, battery)) {
					System.out.println("残バッテリーがMaxバッテリーをオーバーしています！");
					System.out.print("残バッテリー: ");   battery = stdIn.nextDouble();
				}
				else {
					myUAV[i].setBattery(battery);
					break;
				}

			}while(true);

		} loop1:while (true) {
			System.out.println("終了[0...Yes] 飛行[1...Yes]：");
			int inputNumber  = stdIn.nextInt();

			if (inputNumber == 0) {

					for(c=0; c<correct.length; c++) {
					System.out.println("回収した時間:" + correct[c].Ctime + ",枚数:" +correct[c].Allphoto);
					}
					break;

			}
			if (inputNumber == 1) {

				System.out.println("何秒飛行する？ : ");
				double j=0;
				double dtime = stdIn.nextDouble();

				while(j< dtime) {//時間
					L= -1;
					if(kannsoku == 1) {
						System.out.println("AAAAAAAAAAAAA");
						for(i=0; i<myUAV.length; i++) {
							if(myUAV[i].getX() ==-173  && myUAV[i].getY() ==650) {
								System.out.println("2週目Let's go****************************************************");
								kannsoku = kannsoku*(-1);
								ymode = ymode*(-1);
							}
							else if(ymode == 1 && myUAV[i].getY() != 650) {//上向き
								dx = 0;
							    dy = 4;
							    //System.out.println("上向き" +ymode);
							}else if(ymode == -1 && myUAV[i].getY() != 50){
								dx = 0;
								dy = -4;
								//System.out.println("下向き");
							}else if(myUAV[i].getY() == 50 || myUAV[i].getY() == 650 ){
								right = 1;
							}
						}
						if(right == 1) {
							if(kyorimode == 1) {
								if(rightcount == 7) {
									if(ymode==1) {
										dx = 0;
										dy = -4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}else {
										dx = 0;
									    dy = 4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}
								}else {
								dx = 4;
								dy = 0;
								rightcount += 1;
								//System.out.println("right:"+right);
								}
							}else {

								if(rightcount == 7) {
									if(ymode==1) {
										dx = 0;
										dy = -4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}else {
										dx = 0;
									    dy = 4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}
								}else {
								dx = 4;
								dy = 0;
								rightcount += 1;
								//System.out.println("right:"+right);
								}

							}


						}
					}else if(kannsoku == -1) {
						System.out.println("BBBBBBBBBBBBBBBBBB");
						for(i=0; i<myUAV.length; i++) {
							if(myUAV[i].getX()==-285 && myUAV[i].getY() == 50) {
								System.out.println("終了***************************************************************************");
								kannsoku = kannsoku*(-1);
								ymode = ymode*(-1);
								dy += 4;
								System.out.println("right" +right+ "ymode" +ymode+ dy);
							
								System.out.println("rightcount:"+rightcount);
							}
							else if(ymode == 1 && myUAV[i].getY() != 650) {//上向き
								dx = 0;
							    dy = 4;
							    //System.out.println("上向き" +ymode);
							}else if(ymode == -1 && myUAV[i].getY() != 50){
								dx = 0;
								dy = -4;
								//System.out.println("下向き");
							}else if(myUAV[i].getY() == 50 || myUAV[i].getY() == 650 ){
								right = 1;
							}
						}
						if(right == 1) {
							if(kyorimode == 1) {
								if(rightcount == 7) {
									if(ymode==1) {
										dx = 0;
										dy = -4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}else {
										dx = 0;
									    dy = 4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}
								}else {
								dx = -4;
								dy = 0;
								rightcount += 1;
								//System.out.println("right:"+right);
								}
							}else {

								if(rightcount == 7) {
									if(ymode==1) {
										dx = 0;
										dy = -4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}else {
										dx = 0;
									    dy = 4;
										ymode = ymode*(-1);
										right = 0;
										rightcount = 0;
										kyorimode = kyorimode*(-1);
									}
								}else {
								dx = -4;
								dy = 0;
								rightcount += 1;
								//System.out.println("right:"+right);
								}

							}


						}

					}



					if(hoba==1) {

						System.out.println("---------------------------------------------");
						System.out.println("飛行時間:" +(alltime+j+1)+ "s, sensingtime:" +(sensingtime+1)+ "s" );

					}else {

					System.out.println("---------------------------------------------");
					System.out.println("飛行時間:" +(alltime+j+1)+ "s, sensingtime:" +(sensingtime+1)+ "s");
					}
					for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
						double cometime = Math.sqrt( myUAV[i].getX()* myUAV[i].getX() +  myUAV[i].getY()* myUAV[i].getY())/8;  // 帰還時間=移動距離/秒速8m
						if(myUAV[i].move==1) {
							if(myUAV[i].kikan == 2) {//予備機出発


								double gotime = Math.sqrt(myHOBA[l].hobagetX()*myHOBA[l].hobagetX() +  myHOBA[l].hobagetY()*myHOBA[l].hobagetY())/8;

								double dddx= myHOBA[l].hobagetX();
								double	dddy= myHOBA[l].hobagetY();

								System.out.println("dddx" +dddx+ "dddy" +dddy);

									if (myUAV[i].go(1, dddx, dddy, gotime)== 0) {
										System.out.println("燃料が足りません！");
										break loop1;
									}else if( myUAV[i].getY() >= myHOBA[l].hobagetY()) {
										System.out.println("check1");
										myUAV[i].x=myHOBA[l].hobagetX();
										myUAV[i].y=myHOBA[l].hobagetY();
										// 元の位置に戻る
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + myUAV[i].getFuel());
										sensingcount += 1;
										myUAV[i].kikan = 0;
										hobacount += 1;
										System.out.println("hovacount" +hobacount);
										k -= 1;

									}else {
										System.out.println("check2");
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + myUAV[i].getFuel());

									}
									l+=1;

							}else if(myUAV[i].kikan == 1) {//帰還
								double ddx=myUAV[i].getX();
								double	ddy=myUAV[i].getY();
									if (myUAV[i].come(1, ddx, ddy, cometime) == 0) {
										System.out.println("燃料が足りません！");
										break loop1;
									}else if( myUAV[i].getY()<= 0.0){
										myUAV[i].x=0.0;
										myUAV[i].y=0.0;
										// (0,0)に帰還
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + myUAV[i].getFuel());
										movecount -= 1;
										allphoto += myUAV[i].photo;
										myUAV[i].photo = 0;
										System.out.println("飛行時間:" +(alltime+j+1)+ ",allphoto" +allphoto);
										correct[c].Ctime = (alltime+j+1);
										correct[c].Allphoto = allphoto;
										c += 1;
										System.out.println("稼働台数" +movecount);
										myUAV[i].move =0;

									}else {
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + myUAV[i].getFuel());
									}
							}else if(myUAV[i].fly(1, dx, dy) == 0) {
										System.out.println("燃料が足りません！");
							}else{
								myUAV[i].photocount += 1;
								if(myUAV[i].photocount == 8) {
								myUAV[i].photo += 1;
								myUAV[i].photocount =0;
								}
								System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel()))+ "画像枚数" +myUAV[i].getPhoto());

								double threshould = cometime*2*hobacons;//時間*最低燃費
									if(myUAV[i].getFuel()<threshould) {
									System.out.println("活動限界に達しました,(0.0,0.0)に帰還します");
									sensingcount -= 1;
									hobacount -=1;
									//System.out.println("No" +(i+1)+"hobax" +myUAV[i].getX()+ "hobay" +myUAV[i].getY());
									System.out.println("k==============================" +k);
									myHOBA[k].hobax=myUAV[i].getX();
									myHOBA[k].hobay=myUAV[i].getY();
									//System.out.println("k:" +k+ "hobax" +myHOBA[k].hobagetX()+ "hobay" +myHOBA[k].hobagetY());
									System.out.println("k:0hobax" +myHOBA[0].hobagetX()+ "hobay" +myHOBA[0].hobagetY());


										k += 1;

									//System.out.println("k=" +k);
									myUAV[i].kikan = 1;
									thre = 1;

									}
							}

						}else {

							myUAV[i].battery += plusbattery;
							if(myUAV[i].battery >= capa) {
								myUAV[i].battery = capa;
								System.out.println("No" + (i+1) +"充電が完了:");
							}else {
							System.out.println("No" + (i+1) + "充電中...残バッテリー " + myUAV[i].getFuel());
							}



						}
					}
					if(movecount < 1) {
						for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
							if(myUAV[i].battery==capa) {
								System.out.println("予備機にNo" +(i+1)+ "を選択");
								myUAV[i].move = 1;
								myUAV[i].kikan = 2;
								movecount +=1;
								System.out.println("稼働台数" +movecount);
								if(movecount == 1) {
									break;
								}
							}
						}

					}
					if(hobacount == 1) {
						cons = 66.314;
						thre =0;


					}
					if(sensingcount > 0) {
						sensingtime += 1;
					}

				j+= 1;
				l = 0;
				}
				alltime += dtime;
			}

		}
	}
}

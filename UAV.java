/**
 *
 */
package program;

/**
 * @author katsu
 *
 */
public class UAV {
	private String name;    // 名前
	private int number;         // ナンバー
	/*private*/ double x;        // 現在位置Ｘ座標
	/*private*/ double y;        // 現在位置Ｙ座標
	private double capa;        // MAXバッテリー容量
	private double cons;           // 燃費
	double battery;        // 残りバッテリー
	double plusbattery;  // 給油バッテリー
	private int time; //飛行時間
	int kikan;
	int move;
	int photo;
	int photocount;

	// コンストラクタ
	UAV(double x, double y, double capa, double cons, double battery, int kikan, int move, int photo, int photocount) {
	/*this.name = name;       this.number = number;*/  this.capa = capa;
	this.cons = cons;       this.battery = battery;      plusbattery =0;  x = y = 0.0; time = 0;
	kikan = 0; move =0; photo = 0; photocount = 0;
	}

	double getX()    { return x; }    // 現在位置Ｘ座標を取得
	double getY()    { return y; }    // 現在位置Ｙ座標を取得
	double getFuel() { return battery; }    // 残りバッテリーを取得
	int getTime() {return time; }  //飛行時間timeを取得
	int getPhoto() {return photo; }//画像枚数photo取得

	// スペック表示
	void putSpec() {
	//System.out.println("名前：" + name);
	//System.out.println("ナンバー : " + number);
	System.out.println("MAXバッテリー量 : " + capa + "L");
	System.out.println("燃費 : " + cons + "km/L");
	}

	// 残りバッテリーがMAXバッテリーをオーバーしていないか判断
	boolean over(double capa, double battery) {
	if (capa >= battery)
	return true;
	else
	return false;
	}

	// 最初に入っている残りバッテリー量を変更
	double setBattery(double battery) {return this.battery = battery;}

	// Ｘ方向にdx・Ｙ方向にdy移動
	/*int change(double ditme,double dx, double dy) {
	double dist = Math.sqrt(dx * dx + dy * dy);  // 移動距離
	Random rand = new Random();
	int num = rand.nextInt(66);
	cons += num;
	System.out.print("燃費[" +cons + "] ");
	if (dist > battery)
	return 0;                // 移動できない … バッテリー不足
	else {
	battery -= dist/cons; // 移動距離の分だけ燃費に応じて燃料が減る
	x += dx;
	y += dy;
	return 1;                // 移動完了
	}
	}*/
	//基地局へ帰還
	int come(double dtime, double dx, double dy, double cometime) {
		double  flytime = dtime;
		cons = 69.258;
		System.out.print("燃費[" + (String.format("%.2f", cons)) + "/");
		 //Random rand = new Random();
			//int num = rand.nextInt(69);
			//cons += num;
			System.out.print( (String.format("%.2f", cons)) + "] ");
		if(battery < flytime*cons )
			return 0;
		else {
			battery -=  flytime*cons;
			time += dtime;
			//cons -= num;
			x -= dx/cometime;
			y -= dy/cometime;
		return 1;
		}
		}

	int go(double dtime, double dx, double dy, double gotime) {
		double  flytime = dtime;
		cons = 69.258;
		System.out.print("燃費[" +(String.format("%.2f", cons)) + "/");
		 //Random rand = new Random();
			//int num = rand.nextInt(69);
			//cons += num;
			System.out.print( (String.format("%.2f", cons)) + "] ");
		if(battery < flytime*cons )
			return 0;
		else {
			battery -=  flytime*cons;
			time += dtime;
		//	cons -= num;
			x += dx/gotime;
			y += dy/gotime;
		return 1;
		}
		}


	//飛行時間から残バッテリー
	int fly(double dtime, double dx, double dy) {
	double  flytime = dtime;
	cons = 67.082;
	System.out.print("燃費[" +(String.format("%.2f", cons)) + "/");

	// Random rand = new Random();
		//int num = rand.nextInt(69);
		//cons += num;
		System.out.print( (String.format("%.2f", cons)) + "] ");
	if(battery < flytime*cons )
		return 0;
	else {
		battery -=  flytime*cons;
		time += dtime;
	//	cons -= num;
		x += dx;
		y += dy;
	return 1;
	}
	}

	int hobaling(double dtime, double dx, double dy) {
		double  flytime = dtime;
		cons = 66.026;
		System.out.print("燃費[" +(String.format("%.2f", cons)) + "/");
	//	 Random rand = new Random();
		//	int num = rand.nextInt(66);
			//cons += num;
			System.out.print( (String.format("%.2f", cons)) + "] ");
		if(battery < flytime*cons )
			return 0;
		else {
			battery -=  flytime*cons;
			time += dtime;
	//		cons -= num;
			x += dx;
			y += dy;
		return 1;
		}
		}


	// 給油
	//double refueling(double plusbattery) {return this.battery += plusbattery;}

}
